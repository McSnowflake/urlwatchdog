# URL WatchDog

Pings a given URL repeatedly and logs the success in a file.

##  CMD line Options:

`--Log file name, -l [urlLog.txt] -> The name of the file the log will be written to. { String }`

`--log Period, -p [PT30S] -> The time between two ping attempts in ISO format (ex. PT1D2M15S) { String }`

`--Append log, -a [true] -> If set to false the log file will be overwritten on restart`

`--help, -h -> Usage info`