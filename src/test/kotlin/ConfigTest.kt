import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import java.io.File
import kotlin.time.Duration

class ConfigTest : StringSpec() {

    init {
        "setting log file name works" {
            val testLogFilename = "logFile.txt"
            Config(arrayOf("-o", testLogFilename)).logFile shouldBe File(testLogFilename)
        }
        "not setting log file name defaults to" {
            Config(emptyArray()).logFile shouldBe File("urlLog.txt")
        }
        "setting log period works" {
            val testLogPeriod = "PT1H30M45S"
            Config(arrayOf("-p", testLogPeriod)).logPeriod shouldBe Duration.parseIsoString(testLogPeriod)
        }
        "not setting log period defaults to 30 seconds" {
            Config(emptyArray()).logPeriod shouldBe Duration.parseIsoString("PT30S")
        }

        "setting Append log flag works" {
            Config(arrayOf("-a")).appendLog shouldBe true
        }
        "not setting Append log flag defaults to false" {
            Config(emptyArray()).appendLog shouldBe false
        }

    }
}