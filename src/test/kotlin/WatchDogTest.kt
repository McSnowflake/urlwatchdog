import io.kotest.common.ExperimentalKotest
import io.kotest.core.spec.style.StringSpec
import io.kotest.core.test.testCoroutineScheduler
import io.ktor.client.engine.*
import io.ktor.client.engine.mock.*
import io.mockk.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.currentTime
import kotlinx.coroutines.test.runTest
import logging.LoggerInterface
import java.net.URL
import kotlin.time.Duration

@OptIn(ExperimentalCoroutinesApi::class, ExperimentalStdlibApi::class, ExperimentalKotest::class)
class WatchDogTest : StringSpec() {

    init {
        testCoroutineDispatcher = true

        val testUrl = URL("http://www.test.de")
        val duration = Duration.parseIsoString("PT1H")
        val mockEngine: HttpClientEngine = MockEngine { respondOk() }

        val loggerMock: LoggerInterface = mockk {
            coEvery { log(testUrl, any(), any()) } just runs
        }
        "pings per " {

            val testScope = CoroutineScope(testCoroutineScheduler)

            val sut = WatchDog(loggerMock, duration, mockEngine, testScope)
            sut.start(testUrl)
            testCoroutineScheduler.advanceTimeBy(duration.times(3).inWholeMilliseconds)
            sut.stop()
            coVerify(exactly = 4) { loggerMock.log(any(), any(), any()) }
            confirmVerified(loggerMock)
        }
        "pings per time as configured" {
            runTest {

                val testCoroutineScope = CoroutineScope(this.coroutineContext)

                val sut = WatchDog(loggerMock, duration, mockEngine, testCoroutineScope)
                sut.start(testUrl)

                println(currentTime)

                advanceTimeBy(duration.times(3).inWholeMilliseconds)

                println(currentTime)


                confirmVerified(loggerMock)
                coVerify(exactly = 4) { loggerMock.log(any(), any(), any()) }


            }
        }

        "starting and self canceling works" {

            runTest {

                val sut = WatchDog(loggerMock, duration, mockEngine, this)

                sut.start(testUrl)
                delay(50)
                sut.stop()

                confirmVerified(loggerMock)
                coVerify(exactly = 1) { loggerMock.log(testUrl, any(), true) }

            }
        }


    }
}
