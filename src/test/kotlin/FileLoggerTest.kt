import io.kotest.core.spec.style.StringSpec
import io.kotest.core.test.TestCase
import io.kotest.core.test.TestResult
import io.kotest.extensions.time.ConstantNowTestListener
import io.kotest.matchers.shouldBe
import logging.FileLogger
import java.io.File
import java.net.URL
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId

class FileLoggerTest : StringSpec() {

    private val testURL = URL("http://testURL")
    private val testTimestamp = LocalDateTime.ofInstant(Instant.parse("2022-04-14T21:30:33+00:00"), ZoneId.of("UTC"))

    private lateinit var testFile: File

    override suspend fun beforeEach(testCase: TestCase) {
        super.beforeEach(testCase)
        testFile = File("testFile")
    }

    override suspend fun afterEach(testCase: TestCase, result: TestResult) {
        testFile.delete()
        super.afterEach(testCase, result)
    }

    init {

        "logging writes into file successfully" {
            val sut = FileLogger(testFile)
            sut.log(testURL, testTimestamp, true)
            testFile.readText() shouldBe "14-APR-2022 21:30:33 Uhr : http://testURL -> erreichbar!\n"
        }
        "appending to file works successfully" {
            var sut = FileLogger(testFile)
            sut.log(testURL, testTimestamp, false)
            sut = FileLogger(testFile)
            sut.log(testURL, testTimestamp, true)
            testFile.readText() shouldBe """14-APR-2022 21:30:33 Uhr : http://testURL -> nicht erreichbar!
                |14-APR-2022 21:30:33 Uhr : http://testURL -> erreichbar!
                |
            """.trimMargin()
        }
        "overwriting file works successfully" {
            var sut = FileLogger(testFile)
            sut.log(testURL, testTimestamp, false)
            sut = FileLogger(testFile)
            sut.log(testURL, testTimestamp, true)
            testFile.readText() shouldBe "14-APR-2022 21:30:33 Uhr : http://testURL -> erreichbar!\n"
        }
    }
}