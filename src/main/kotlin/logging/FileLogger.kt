package logging

import kotlinx.coroutines.runInterruptible
import java.io.File
import java.io.FileWriter
import java.net.URL
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder
import java.time.format.TextStyle
import java.util.Locale.GERMAN

class FileLogger(private val logFile: File) : LoggerInterface {

    private val formatter: DateTimeFormatter =
        DateTimeFormatterBuilder().appendPattern("HH:mm:ss").toFormatter()

    override suspend fun log(url: URL, timestamp: LocalDateTime, success: Boolean) {
        runInterruptible {
            FileWriter(logFile, true).use {
                val message = createLogMessage(url, timestamp, success)
                it.appendLine(message)
            }
        }
    }

    private fun createLogMessage(url: URL, timestamp: LocalDateTime, success: Boolean): String {
        val monthString = timestamp.month.getDisplayName(TextStyle.FULL, GERMAN).uppercase().substring(0, 3)
        val timestampString = "${timestamp.dayOfMonth}-$monthString-${timestamp.year} ${formatter.format(timestamp)}"
        return "$timestampString Uhr : $url -> ${if (!success) "nicht " else ""}erreichbar!"
    }

}