package logging

import java.net.URL
import java.time.LocalDateTime

interface LoggerInterface {
    suspend fun log(url: URL, timestamp: LocalDateTime, success: Boolean)
}