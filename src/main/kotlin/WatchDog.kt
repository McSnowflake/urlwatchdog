import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.*
import logging.LoggerInterface
import java.net.URL
import java.time.LocalDateTime
import kotlin.time.Duration

class WatchDog(
    private val loggerInterface: LoggerInterface,
    private val pingPeriod: Duration,
    engine: HttpClientEngine = CIO.create(),
    private val coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.IO)
) {

    private val client = HttpClient(engine) {
        install(HttpTimeout) { requestTimeoutMillis = pingPeriod.inWholeMilliseconds }
    }


    fun start(url: URL) {

        // potential flow based solution
//        flow {
//            while (true) {
//                emit(url)
//                delay(pingPeriod)
//
//            }
//        }
//            .onEach { ping(url) }
//            .launchIn(coroutineScope)

        coroutineScope.launch {
            while (true) {
                coroutineScope.launch { ping(url) }
                delay(pingPeriod)
            }
        }
    }

    fun stop() = coroutineScope.cancel()

    private suspend fun ping(url: URL) {
        val timeStamp = LocalDateTime.now()!!
        kotlin.runCatching { client.get(url) }
            .onFailure { loggerInterface.log(url, timeStamp, false) }
            .onSuccess { loggerInterface.log(url, timeStamp, it.status.isSuccess()) }
    }
}