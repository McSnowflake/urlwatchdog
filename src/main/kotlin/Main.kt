import logging.FileLogger
import ui.ConsoleUserInterface

fun main(args: Array<String>) {

    val config = Config(args)
    val logger = FileLogger(config.logFile)
    val watchDog = WatchDog(logger, config.logPeriod)

    val url = ConsoleUserInterface.askForUrl()
    watchDog.start(url)

    ConsoleUserInterface.waitForUserStop(url)
    watchDog.stop()
}