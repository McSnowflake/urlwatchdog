package ui

import java.net.URL

object ConsoleUserInterface : UserInterface {

    override fun askForUrl(): URL {
        println("Bitte geben Sie die URL ein, die überwacht werden soll.")
        var urlString = readLine()

        while (urlString.isNotValidUrl()) {
            println("Die eingegeben URL entspricht nicht dem erwarteten Format (${WebUrlPattern.pattern})")
            println("Bitte geben Sie die URL ein, die überwacht werden soll.")

            urlString = readLine()
        }
        return URL(urlString)
    }

    override fun waitForUserStop(url: URL) {
        println("$url wird überwacht, drücken Sie eine beliebige Taste um das Programm zu beenden...")
        System.`in`.read()
        println("Stopping ...")
    }
}

val WebUrlPattern =
    Regex("https?://(www\\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()!@:%_+.~#?&/=]*)")

private fun String?.isNotValidUrl() = this.isNullOrBlank() || !WebUrlPattern.matches(this)
