package ui

import java.net.URL

interface UserInterface {
    fun askForUrl(): URL
    fun waitForUserStop(url: URL)
}