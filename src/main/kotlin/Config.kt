import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.default
import java.io.File
import kotlin.time.Duration

const val LogFileName = "urlLog.txt"
const val LogPeriodISO = "PT30S"

class Config(args: Array<String>) {

    private val parser = ArgParser("UrlWatchDog")
    private val logFileName by parser.option(
        ArgType.String,
        shortName = "o",
        fullName = "output",
        description = "The name of the file the log will be written to."
    ).default(LogFileName)
    private val logPeriodISO by parser.option(
        ArgType.String,
        shortName = "p",
        fullName = "period",
        description = "The time between two ping attempts in ISO format (ex. PT1D2M15S)"
    ).default(LogPeriodISO)
    val appendLog: Boolean by parser.option(
        ArgType.Boolean,
        shortName = "a",
        fullName = "append",
        description = "If set, the log file will be not be overwritten, but expanded."
    ).default(false)

    val logFile: File
    val logPeriod: Duration

    init {
        parser.parse(args)
        logFile = File(logFileName)
        if (!appendLog) {
            logFile.delete()
            logFile.createNewFile()
        }
        logPeriod = Duration.parseIsoString(logPeriodISO)
    }

}